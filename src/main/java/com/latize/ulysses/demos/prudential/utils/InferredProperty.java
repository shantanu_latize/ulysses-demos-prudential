package com.latize.ulysses.demos.prudential.utils;

public enum InferredProperty {

	CUSTOMER_AGE(":customerAge", "age of the customer"),
	YEARS_SINCE_DEATH(":customerYearsSinceDeath", "years since death"),
	YEARS_AS_CUSTOMER(":customerYearsAsCustomer", "years as customer"),
	YEARS_SINCE_FIRST_POLICY_PURCHASE(":customerYearsSinceFirstPolicyPurchase", "years since first policy purchase"),
	YEARS_SINCE_LAST_POLICY_PURCHASE(":customerYearsSinceLastPolicyPurchase", "years since last policy purchase"),
	YEARS_SINCE_POLICY_PROPOSAL(":policyYearsSincePolicyProposal", "years since policy was proposed"),
	YEARS_SINCE_POLICY_INCEPTION(":policyYearsSincePolicyInception", "years since policy inception"),
	YEARS_SINCE_POLICY_PAIDTO(":policyYearsSincePolicyPaidTo", "years since the policy was paid to"),
	YEARS_SINCE_COMPONENT_RISK_CEASED(":policyYearsSinceComponentRiskCeased", "years since component risk ceased"),
	YEARS_SINCE_COMPONENT_RISK_COMMENCED(":policyYearsSinceComponentRiskCommenced", "years since commencement of component risk"),
	YEARS_SINCE_COMPONENT_COVERAGE_RISK_COMMENCED(":policyYearsSinceComponentCoverageRiskCommenced",  "years since commenced of component coverage risk"),
	YEARS_SINCE_STATUS_DATE(":policyYearsSinceStatusDate", "years since status changed"),
	YEARS_SINCE_PROPOSAL_DATE(":businessYearsSinceProposalDate", "years since date of proposal"),
	YEARS_SINCE_PROPOSAL_DATE_2(":businessYearsSinceProposalDate2", "years since second proposal date"),
	YEARS_SINCE_TRANSACTION_DATE(":businessYearsSinceTransactionDate", "years since the date of transaction"),
	YEARS_SINCE_COMPONENT_STATUS_DATE(":componentYearsSinceComponentStatusDate", "years since the component status date");
	
	private final String uri;
	private final String label;
	
	private InferredProperty(String uri, String label) {
		this.uri = uri;
		this.label = label;
	}
	
	public String getURI() {
		return uri;
	}
	
	public String getLabel() {
		return label;
	}
	
	public static String getLabelFromURI(String uri) {
		String label = "";
		for(InferredProperty p : InferredProperty.values()) {
			if(p.getURI().equals(uri)) {
				label = p.getLabel();
				break;
			}
		}
		return label;
	}
}
