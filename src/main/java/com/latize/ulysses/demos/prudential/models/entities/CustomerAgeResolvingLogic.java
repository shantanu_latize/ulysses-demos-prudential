package com.latize.ulysses.demos.prudential.models.entities;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.marmotta.commons.vocabulary.XSD;
import org.apache.marmotta.platform.core.util.DateUtils;

import com.latize.ulysses.demos.prudential.utils.InferredProperty;
import com.latize.ulysses.recommender.datatypes.Triple;
import com.latize.ulysses.recommender.models.GenericRecommenderModel;
import com.latize.ulysses.recommender.models.ProfileValueModel;
import com.latize.ulysses.recommender.models.ResolvedEntityModel;
import com.latize.ulysses.recommender.utils.RecommenderConstants;

public class CustomerAgeResolvingLogic extends GenericRecommenderModel {
	
	public static final String URI = String.format(RecommenderConstants.PROPERTY_PATH_FORMAT, "http://data.latize.com/vocab/dob", XSD.String.stringValue());

	@Override
	public List<ResolvedEntityModel> disambiguate() {
		return null;
	}

	@Override
	public List<ResolvedEntityModel> applyExternalLogic(Triple<String, String, String> triple) {
		// TODO Auto-generated method stub
		
		List<ResolvedEntityModel> resolvedEntities = new ArrayList<>();
		
		try {
			Date d = DateUtils.getParsedDate(triple.getThird(), "yyyy-MM-dd");
			ResolvedEntityModel r1 = new ResolvedEntityModel();
			String label = getMonthAndYearOfDate(d);
			
			r1.setUri(label);
			r1.setType(XSD.DateTime.stringValue());
			r1.setTypeLabel(ProfileValueModel.TYPE.STRING.name());
			r1.setPropertyURI(triple.getSecond());
			resolvedEntities.add(r1);
			
			// Getting the customer age
			ResolvedEntityModel r2 = new ResolvedEntityModel();
			int numYears = getNumYearsBetween(new Date(), d); // Age of the person
			r2.setUri(String.valueOf(numYears));
			r2.setType(XSD.Integer.stringValue());
			r2.setTypeLabel(ProfileValueModel.TYPE.NUMERIC.name());
			r2.setPropertyURI(InferredProperty.CUSTOMER_AGE.getURI());
			r2.setPropertyLabel(InferredProperty.CUSTOMER_AGE.getLabel());
			resolvedEntities.add(r2);
			// ---
		} catch(ParseException ex) {
			// Do nothing
		}
		
		return resolvedEntities;
	}

}
