package com.latize.ulysses.demos.prudential.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.inject.Inject;

import org.apache.marmotta.platform.core.api.config.ConfigurationService;
import org.apache.marmotta.platform.core.util.CryptoUtils;

import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import com.latize.euphoria.exception.RecommendationEngineException;
import com.latize.euphoria.models.VisualizationModel;
import com.latize.euphoria.models.VisualizationStateModel;
import com.latize.ulysses.platform.classifiers.models.RecommendationModel;
import com.latize.ulysses.platform.classifiers.models.RecommendationParameterModel;
import com.latize.ulysses.recommender.apis.RecommendationService;
import com.latize.ulysses.recommender.exceptions.RecommenderException;
import com.latize.ulysses.recommender.impls.SemanticProfileBuilder;

public class PrudentialPolicyRecommendationService implements RecommendationService {
	
	@Inject
	private ConfigurationService configService;
	
	@Inject
	private SemanticProfileBuilder profileBuilder;
	
//	private static final String CustomerClassURI = "http://data.latize.com/vocab/Customer";
//	private static final String ItemClassURI = "http://data.latize.com/vocab/Policy";
//	private static final String PropertyPathFromCustomerToItem = "<http://data.latize.com/vocab/ownsPolicies>";
	
	public static final String PROFILE_FILE_NAME = "prudentialrecommender.model";
	
	@PostConstruct
	private void init() {
//		profileBuilder = new SemanticProfileBuilder();
	}
	
	public PrudentialPolicyRecommendationService() {
//		init();
	}

	@Deprecated
	@Override
	public String getEngineName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RecommendationParameterModel getEngineParams() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VisualizationModel getVisualizationDetails() throws RecommendationEngineException {
		VisualizationModel visualization = new VisualizationModel();
		visualization.setName(getEngineName());
		
		Map<String, String> variables = new HashMap<>();
		variables.put("variable", "http://data.latize.com/resource/Student/c8c076d5-8653-8ee4-d16c-c35191e2f049");
		variables.put("subject", "http://data.latize.com/vocab/Student");
		variables.put("label", "JANE DOE");
		variables.put("vizKey", "u4ya_analysis_1234567890");
		visualization.setVars(variables);
		
		try {
			VisualizationStateModel s = new VisualizationStateModel();
			s.setRecommendationEngine(CryptoUtils.encrypt(this.getClass().getCanonicalName()));
			s.setType(VisualizationStateModel.TYPE.RECOMMENDATION);
			visualization.setWorkflow(s);
		} catch (InvalidKeyException e) {
			throw new RecommendationEngineException(e, this.getClass());
		} catch (NoSuchAlgorithmException e) {
			throw new RecommendationEngineException(e, this.getClass());
		} catch (InvalidKeySpecException e) {
			throw new RecommendationEngineException(e, this.getClass());
		} catch (NoSuchPaddingException e) {
			throw new RecommendationEngineException(e, this.getClass());
		} catch (InvalidAlgorithmParameterException e) {
			throw new RecommendationEngineException(e, this.getClass());
		} catch (IllegalBlockSizeException e) {
			throw new RecommendationEngineException(e, this.getClass());
		} catch (BadPaddingException e) {
			throw new RecommendationEngineException(e, this.getClass());
		} catch (UnsupportedEncodingException e) {
			throw new RecommendationEngineException(e, this.getClass());
		}
		
		return visualization;
	}

	@Override
	public List<RecommendationModel> getRecommendations(String instanceURI) throws RecommendationEngineException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void buildSemanticProfile() throws RecommendationEngineException {
		profileBuilder.setEntityResolvers(getEntityResolvers());
		
		try {
			profileBuilder.setEngineClass(getClass());
			System.out.println(String.format("In %s. Building profile.", getClass().getCanonicalName()));
			profileBuilder.buildProfile(configService.getStringConfiguration("uri.customerclass"), configService.getStringConfiguration("uri.itemclass"), configService.getStringConfiguration("uri.recommender.propertypath"));
		} catch (RecommenderException e) {
			throw new RecommendationEngineException(e, getClass());
		}
	}
	
	private Map<String, Class<?>> getEntityResolvers() throws RecommendationEngineException {
		Map<String, Class<?>> entityResolvers = new HashMap<>();
		
		try {
			ClassPath classpath = ClassPath.from(Thread.currentThread().getContextClassLoader());
			
			ImmutableSet<ClassPath.ClassInfo> ctxModels = classpath.getTopLevelClasses("com.latize.ulysses.demos.prudential.models.entities");
			
			for(ClassPath.ClassInfo m : ctxModels) {
				Class<?> modelClass = m.load();
				entityResolvers.put((String)modelClass.getField("URI").get(null), modelClass);
			}
		} catch (IOException e) {
			throw new RecommendationEngineException("Not able to get the entity resolvers for Prudential", getClass());
		} catch (IllegalArgumentException e) {
			throw new RecommendationEngineException("Invalid field name provided.", getClass());
		} catch (IllegalAccessException e) {
			throw new RecommendationEngineException("Unable to access the field URI in the entity resolver", getClass());
		} catch (NoSuchFieldException e) {
			throw new RecommendationEngineException("Field URI does not exist in entity resolver", getClass());
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return entityResolvers;
	}

}
