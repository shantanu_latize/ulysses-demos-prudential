package com.latize.ulysses.demos.prudential.helpers;

import java.util.Date;
import java.util.Random;
import java.util.UUID;

import org.apache.marmotta.platform.core.util.DateUtils;

public class TestHelper {

	public static int getRandomIntegerBetween(int min, int max) {
		Random r = new Random();
		return r.nextInt(max - min) + min;
	}
	
	public static long getRandomLongBetween(long min, long max) {
		Random r = new Random();
		return (min + (long) r.nextDouble() * (max - min));
	}
	
	public static double getRandomDoubleBetween(double min, double max) {
		Random r = new Random();
		
		double rnd = min + (max - min) * r.nextDouble();
		if(Double.valueOf(rnd).isInfinite()) {
			return Double.POSITIVE_INFINITY;
		}
		
		return rnd;
	}
	
	public static String getRandomString() {
		return UUID.randomUUID().toString();
	}
	
	public static String getRandomStringFrom(String[] collectionOfStrings) {
		int min = 0;
		int max = collectionOfStrings.length - 1;
		
		int rnd = getRandomIntegerBetween(min, max);
		
		return collectionOfStrings[rnd];
	}
	
	public static Date getRandomDateBetween(Date min, Date max) {
		long ts1 = min.getTime();
		long ts2 = max.getTime();
		
		long rnd = getRandomLongBetween(ts1, ts2);
		
		return new Date(rnd);
	}
	
	public static String getRandomFormattedDate(Date min, Date max) {
		Date randomDate = getRandomDateBetween(min, max);
		
		return DateUtils.getFormattedDate(randomDate, "yyyy-MM-dd");
	}
}
